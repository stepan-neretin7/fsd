#include <iostream>
#include <fstream>
#include <tuple>
#include "CSVParser.h"
#include "Utility.h"

int main() {
  try {
	std::ifstream file("in.csv");
	auto parser = CSVParser<int, std::string>(std::move(file));


	for (const std::tuple<int, std::string> &rs : parser) {
	  std::cout << rs;
	}
  }
  catch (const std::exception &e) {
	printWhat(e);
	return 1;
  }

  return 0;
};